<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id', 'name', 'surname', 'fc', 'phone', 'address'
    ];

    public function profile()
    {
        return $this->belongsTo('App\User');
    }
}
