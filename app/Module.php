<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'code', 'artist_name', 'song_name', 'duration', 'genre'
    ];

    /**
     * Define n-n relationship with \User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')
            ->withTimestamps();
    }

    /**
     * Define n-n relationship with \Upload
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function uploads()
    {
        return $this->belongsToMany('App\Upload')
            ->withTimestamps();
    }
}
