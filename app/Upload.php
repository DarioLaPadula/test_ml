<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $fillable = [
        'success', 'note'
    ];

    public function profile()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Define n-n relationship with \Module
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function modules()
    {
        return $this->belongsToMany('App\Module')
            ->withTimestamps();
    }

    /**
     * Define n-n relationship with \User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')
            ->withTimestamps();
    }
}
