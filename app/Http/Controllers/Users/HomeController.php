<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\ModuleRequest;
use App\Imports\ModulesImport;
use App\Profile;
use App\User;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use PhpOffice\PhpSpreadsheet\Exception;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $profile = Profile::where('user_id', auth()->user()->id)->first();
        if (!$profile) {
            $profile = new Profile();
        }

        return view('users.home', [
            'profile' => $profile
        ]);
    }

    public function store(ModuleRequest $request)
    {
        try {
            Excel::import(new ModulesImport(), $request->file('file'));
        } catch (Exception $pse) {
            return back()->withErrors(['file' =>'Formato excel non valido']);
        }
        $profile = Profile::firstOrNew(['user_id' => auth()->user()->id]);
        $profile->fill($request->all())->save();

        return response()->redirectToRoute('users.summary')
            ->with('success', 'Dati inviati con successo');
    }

    public function summary()
    {
        $user = User::find(auth()->user()->id);

        return view('users.summary', [
            'user' => $user,
            'profile' => $user->profile()->first(),
            'uploads' => $user->uploads()->first(),
        ]);
    }

    public function make_pdf()
    {
        $user = User::find(auth()->user()->id);

        return PDF::loadView('users.pdf', [
            'user' => $user,
            'profile' => $user->profile()->first(),
            'uploads' => $user->uploads()->first(),
        ])->setOptions(['images' => true])
            ->stream();
    }


}
