<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'numeric|nullable|exists:user,id',
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'fc' => 'required|string|min:16|max:16',
            'phone' => 'required|string',
            'phone' => 'required|string',
            'address' => 'nullable|string',
            'note' => 'nullable|string',
            'file' => 'required|max:30720|mimes:xls',

        ];
    }
}
