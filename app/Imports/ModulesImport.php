<?php

namespace App\Imports;

use App\Module;
use App\Upload;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;

class ModulesImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), $this->__rules(), $this->__validationMessages())->validate();

        $upload = Upload::create([
            'success' => 1,
            'note' => request()->note,
        ]);

        $upload->users()->attach(auth()->user()->id);

        foreach ($rows as $row) {

            $toInsert = [
                'artist_name' => $row['nome_artista'],
                'song_name' => $row['nome_brano'],
                'duration' => $row['durata'],
                'genre' => $row['genere'],
            ];

            $module = Module::where('code', $row['codice_brano'])->first();

            if ($module) {
                $module->update($toInsert);
            } else {
                $module = Module::create(array_merge(['code' => $row['codice_brano']], $toInsert));
            }
            $module->users()->sync(auth()->user()->id);
            $module->uploads()->sync($upload->id);
        }
    }

    private function __rules()
    {
        return [
            '*.codice_brano' => 'required',
            '*.nome_artista' => 'required',
            '*.nome_brano' => 'required',
            '*.durata' => 'required|numeric',
            '*.genere' => 'required'
        ];
    }

    private function __validationMessages()
    {
        return [
            'required' => 'Formato excel non valido',
            'numeric' => 'Formato excel non valido',
        ];
    }
}
