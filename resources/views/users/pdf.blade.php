<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body style="margin: 40px">

<p style="font-size: 32px">RIEPILOGO RICHIESTE UTENTE - {{ $user->email }}</p>

<div>
    <p style="font-size: 24px; margin-bottom: 20px">Dati Anagrafici</p>
    Nome: {{ $profile->name }}<br>
    Cognome: {{ $profile->surname }}<br>
    Codice Fiscale: {{ $profile->cf }}<br>
    Telefono: {{ $profile->phone }}<br>
    @if ($profile->address)
        <div class="col-xs-12">
            Indirizzo: {{ $profile->address }}
        </div>
    @endif

    <p style="font-size: 24px">Riepilogo upload</p>

    @foreach($user->uploads()->orderBy('created_at', 'desc')->get() as $upload)
        @if (!$upload->modules()->get()->isEmpty())
            <strong>{{ \Carbon\Carbon::parse($upload->created_at)->format('d-m-Y H:i') }}</strong>
            @if ($upload->note)
                <p>Note: {{ $upload->note }}</p>
            @endif

            @foreach($upload->modules()->get() as $module)
                <p style="margin-bottom: 20px">
                    Codice Brano: {{ $module->code }}<br>
                    Nome Artista: {{ $module->artist_name }}<br>
                    Nome Brano: {{ $module->song_name }}<br>
                    Durata: {{ $module->duration }}<br>
                    Genere: {{ $module->genre }}<br>
                </p>
            @endforeach
        @endif
    @endforeach
</div>

</body>
</html>