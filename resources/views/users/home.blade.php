@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->

                <form method="POST" role="form" data-toggle="validator" action="{{ route('users.store') }}"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <h4>Invia file</h4>
                        <div class="form-group">
                            <label for="fc">Codice Fiscale</label>
                            <input type="text" class="form-control" data-minlength="16" maxlength="16"
                                   name="fc" value="{{ old('fc', $profile->fc) }}" required>
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('fc') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control"
                                   name="name" value="{{ old('name', $profile->name) }}" required>
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Cognome</label>
                            <input type="text" class="form-control"
                                   name="surname" value="{{ old('surname', $profile->surname) }}"
                                   required>
                            @if ($errors->has('surname'))
                                <span class="text-danger">{{ $errors->first('surname') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Telefono</label>
                            <input type="text" class="form-control"
                                   name="phone" value="{{ old('phone', $profile->phone) }}" required>
                            @if ($errors->has('phone'))
                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Indirizzo</label>
                            <input type="text" class="form-control"
                                   name="address" value="{{ old('address', $profile->address) }}">
                            @if ($errors->has('address'))
                                <span class="text-danger">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Note</label>
                            <textarea type="text" class="form-control"
                                      name="note">{{ old('note') }}</textarea>
                            @if ($errors->has('note'))
                                <span class="text-danger">{{ $errors->first('note') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Invia File</label>
                            <input type="file" id="file" class="form-control" onchange="ValidateSingleInput(this);"
                                   name="file" required>
                            @if ($errors->has('file'))
                                <span class="text-danger">{{ $errors->first('file') }}</span>
                            @endif
                            @foreach($errors->messages() as $message)
                                @if(in_array('Formato excel non valido' ,$message))
                                    <span class="text-danger">Formato excel non valido</span>
                                @endif
                                @break
                            @endforeach
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 text-right">
                                <button class="btn btn-success btn-submit">Invia</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/bootstrap-validator.min.js') }}"></script>
    <script>
        var _validFileExtensions = [".xls"];
        var _validFileType = ["application/vnd.ms-excel"];
        var _maxSize = 3000 * 1024;

        function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var fp = $("#file");
                var items = fp[0].files;
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var validExt = false;
                    var validType = false;
                    var message = "";
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            validExt = true;
                            break;
                        }
                    }
                    if (validExt == false) {
                        message += "Estensione file non valida\n";
                    }
                    for (var j = 0; j < _validFileType.length; j++) {
                        if (_validFileType[j] == items[0].type) {
                            validType = true;
                            break;
                        }
                    }
                    if (validType == false) {
                        message += "Formato file non valido\n";
                    }

                    if (items[0].size > _maxSize) {
                        message += "Dimensione file non valida, max 3GB";
                    }

                    if (message.length > 0) {
                        alert(message);
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }

        $(document).ready(function () {
            $('form').validator();
        });
    </script>
@endsection