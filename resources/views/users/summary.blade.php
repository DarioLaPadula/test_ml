@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Riepilogo Utente</h1>
@stop

@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- /.box-header -->
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" style="margin-top: 10px">
                        <div>{{ $message }}</div>
                    </div>
                @endif

                <div class="box-body">
                    <h3>Dati Anagrafici</h3>
                    <div class="row">
                        <div class="col-xs-12">
                            Nome: {{ $profile->name }}
                        </div>
                        <div class="col-xs-12">
                            Cognome: {{ $profile->surname }}
                        </div>
                        <div class="col-xs-12">
                            Codice Fiscale: {{ $profile->cf }}
                        </div>
                        <div class="col-xs-12">
                            Telefono: {{ $profile->phone }}
                        </div>
                        @if ($profile->address)
                            <div class="col-xs-12">
                                Indirizzo: {{ $profile->address }}
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Riepilogo upload</h3>
                        </div>

                        @foreach($user->uploads()->orderBy('created_at', 'desc')->get() as $upload)
                            @if (!$upload->modules()->get()->isEmpty())
                                <div class="col-xs-12">
                                    <h5>
                                        <strong>{{ \Carbon\Carbon::parse($upload->created_at)->format('d-m-Y H:i') }}</strong>
                                    </h5>
                                    @if ($upload->note)
                                        <p>Note: {{ $upload->note }}</p>
                                    @endif
                                </div>
                                @foreach($upload->modules()->get() as $module)
                                    <div class="col-xs-12">
                                        <p>
                                            Codice Brano: {{ $module->code }}<br>
                                            Nome Artista: {{ $module->artist_name }}<br>
                                            Nome Brano: {{ $module->song_name }}<br>
                                            Durata: {{ $module->duration }}<br>
                                            Genere: {{ $module->genre }}<br>
                                        </p>
                                    </div>
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <a target="_blank" class="btn btn-primary"
                               href="{{ route('users.make_pdf') }}">Scarica PDF</a>
                            <a class="btn btn-primary"
                               href="{{ route('users.dashboard') }}">Torna indietro</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection