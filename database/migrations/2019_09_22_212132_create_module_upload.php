<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_upload', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('module_id')->references('id')->on('modules');
            $table->unsignedInteger('upload_id')->references('id')->on('uploads');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_upload');
    }
}
