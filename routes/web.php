<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\RegisterController@showRegistrationForm')->name('register');

Auth::routes();

Route::get('home', 'Users\HomeController@dashboard')->name('users.dashboard');
Route::post('store', 'Users\HomeController@store')->name('users.store');
Route::get('summary', 'Users\HomeController@summary')->name('users.summary');
Route::get('make_pdf', 'Users\HomeController@make_pdf')->name('users.make_pdf');

Route::get('model/download', function () {
        return response()->download(storage_path('model.xls'));
});